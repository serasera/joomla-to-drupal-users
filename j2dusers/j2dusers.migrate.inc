<?php
/**
 * @file
 * Declares our migrations.
 */


/**
 * Implements hook_migrate_api().
 */
function j2dusers_migrate_api() {
    $api = array(
        'api' => 2,
        'groups' => array(
            'j2d' => array(
                'title' => t('Joomla to drupal  migration'),
            ),
        ),

        'migrations' => array(
            'j2dusers' => array(
                'class_name' => 'MigrateJ2dUsers',
                'group_name' => 'j2d',
            ),
        ),
    );
    return $api;
}